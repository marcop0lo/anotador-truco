var teamName1;
var teamName2;
var maxPoints = 30;

function initGame() {
    teamName1 = document.getElementById("team-name-1").value;
    teamName2 = document.getElementById("team-name-2").value;
    if (teamName1 && teamName2) {
        document.getElementById("first-page").classList.add("inactive");
        document.getElementById("second-page").classList.remove("inactive");
        document.getElementById("title-name-1").innerHTML = teamName1;
        document.getElementById("title-name-2").innerHTML = teamName2;
        document.getElementById("limit-points").innerHTML = maxPoints;
    } else {
        document.getElementsByClassName("error-input")[0].classList.remove("inactive");
    }
}

function finishGame() {
    document.getElementById("first-page").classList.remove("inactive");
    document.getElementById("second-page").classList.add("inactive");
    teamName1 = document.getElementById("team-name-1").value = "";
    teamName2 = document.getElementById("team-name-2").value = "";
}

function selectPoints(points, other) {
    if (points !== maxPoints) {
        maxPoints = points;
        document.getElementById(points.toString()).classList.remove("inactive-button");
        document.getElementById(other.toString()).classList.add("inactive-button");
    }
}